# [Источник](https://www.kaggle.com/deepu1109/star-dataset) 
Краткое описание: Это набор данных, состоящий из нескольких признаков звезд.  

Суть задачи: доказать, что звезды следуют определенному графику в небесном пространстве,
специально называемому диаграммой Герцшпрунга-Рассела или просто HR-диаграммой, чтобы мы могли классифицировать звезды, построив их характеристики на основе этого графика.  

Целевой признак: Spectral Class.  

Признаки объектов, описание этих признаков, тип:  
Temperature (in K) – Температура поверхности звезды, тип: целочисленный;  
Luminosity (L/Lo) – Светимость звезды, тип: вещественный;  
Radius (R/Ro) – Радиус звезды, тип: вещественный;  
Absolute Magnitude (Mv) – Абсолютная звездная величина, тип: вещественный;  
Star Type – Тип зведы, тип: целочисленный;  
Star Color – Цвет звезды, тип: категориальный;  
Spectral Class – Спектральный класс звезды, тип: категориальный.  
